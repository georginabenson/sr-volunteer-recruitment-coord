Hi, 

Thanks for registering an interest in volunteering for Student Robotics.
You are receiving this email because you signed up on the Student Robotics volunteering page.
This volunteering opportunity is for mentoring of teams, however if this does not interest you we will be in contact in the future with further volunteering opportunities.

As you are possibly already aware, Student Robotics is a UK based organisation that challenges teams of 16-18 year olds to design, build and develop autonomous robots to compete in our annual competition.
As part of this we aim to assist teams throughout the six month development process through mentoring.
Mentors are generally engineering or science undergraduates, graduates or professionals; although anyone with the skills necessary to further the teams' learning and understanding of engineering, science and team work is more than welcome.

Mentors must be 18 years old or over and we are currently only looking for Mentors for our UK teams. A Mentor's main activity is to visit teams to help provide support and advice, so close proximity to a team is helpful. 

We are always actively looking for new Mentors so if you are interested in becoming a Mentor for Student Robotics, then please get in touch by replying to this email.
If you could let us know whereabouts you are in the country, we will endeavour to put you in contact with a local team. 

If you have any other questions regarding Mentoring then please feel free to reply to discuss things further.

Many Thanks,
Georgina Benson
Student Robotics Volunteer Recruitment Coordinator